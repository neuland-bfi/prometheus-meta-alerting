use std::collections::HashMap;
use std::default;

use clap::Parser;

use crate::utils;

pub static ALLOW_PERIODS: bool = false;
pub static IGNORE_META_ALERTING_LABEL: &str = "ignore_meta_alerting";

#[derive(Parser, Debug)]
#[clap(version, about)]
pub struct Cli {
    /// Name for generated alerting rules
    #[clap(long = "meta-alerting-name", default_value_t=String::from(Config::default().meta_alerting_name()))]
    meta_alerting_name: String,
    /// Prefix used for the alert name
    #[clap(long = "metric-absent-prefix", default_value_t=String::from(Config::default().metric_absent_prefix()))]
    metric_absent_prefix: String,
    /// Label (key=value) in addition to the defaults
    #[clap(long = "label")]
    labels: Vec<String>,
    /// Don't automatically add the default labels to the generated alerts
    #[clap(long = "exclude-default-labels")]
    exclude_default_labels: bool,
    /// Annotation (key=value) to be in addition to the defaults
    #[clap(long = "annotation")]
    annotations: Vec<String>,
    /// Don't automatically add the default annotations to the generated alerts
    #[clap(long = "exclude-default-annotations")]
    exclude_default_annotations: bool,
    /// Override the default `for` value used for the generated alerts. Pass an empty string or '-', to disable `for`.
    #[clap(long = "for")]
    duration: Option<String>,
    /// Path to a file containing Prometheus alerting rules
    #[clap(required(true))]
    rules_files: Vec<String>,
    /// Meta rules file. Pass '-' to print to stdout
    output: String,
}

/// Configuration for optional/configurable things like annotations etc.
#[derive(Debug)]
pub struct Config {
    rules_files: Vec<String>,
    output: String,
    meta_alerting_name: String,
    metric_absent_prefix: String,
    labels: HashMap<String, String>,
    annotations: HashMap<String, String>,
    duration: Option<String>,
}
impl Config {
    pub fn from_cli() -> Self {
        let args = Cli::parse();
        Self::from_cli_args(&args)
    }

    fn from_cli_args(args: &Cli) -> Self {
        let default_config = Config::default();

        let mut labels: HashMap<String, String> = args
            .labels
            .iter()
            .filter_map(|labelarg| utils::divide_key_value_arg(labelarg))
            .collect();
        if !args.exclude_default_labels {
            for (default_label, default_value) in default_config.labels() {
                labels.entry(default_label).or_insert(default_value);
            }
        }

        let mut annotations: HashMap<String, String> = args
            .annotations
            .iter()
            .filter_map(|annotationarg| utils::divide_key_value_arg(annotationarg))
            .collect();
        if !args.exclude_default_annotations {
            for (default_annotation, default_value) in default_config.annotations() {
                annotations
                    .entry(default_annotation)
                    .or_insert(default_value);
            }
        }

        let duration = match &args.duration {
            Some(duration) => match duration.as_ref() {
                "" | "-" => None,
                _ => Some(String::from(duration)),
            },
            None => Config::default().duration(),
        };

        Config {
            rules_files: args.rules_files.to_owned(),
            output: args.output.to_owned(),
            meta_alerting_name: args.meta_alerting_name.to_owned(),
            metric_absent_prefix: args.metric_absent_prefix.to_owned(),
            labels,
            annotations,
            duration,
        }
    }

    #[cfg(test)]
    pub fn new(
        rules_files: Vec<String>,
        output: String,
        meta_alerting_name: String,
        metric_absent_prefix: String,
        labels: HashMap<String, String>,
        annotations: HashMap<String, String>,
        duration: Option<String>,
    ) -> Config {
        Config {
            rules_files,
            output,
            meta_alerting_name,
            metric_absent_prefix,
            labels,
            annotations,
            duration,
        }
    }

    pub fn rules_files(&self) -> Vec<String> {
        self.rules_files.to_owned()
    }

    pub fn output(&self) -> &str {
        &self.output
    }

    pub fn meta_alerting_name(&self) -> &str {
        &self.meta_alerting_name
    }

    pub fn metric_absent_prefix(&self) -> &str {
        &self.metric_absent_prefix
    }

    pub fn labels(&self) -> HashMap<String, String> {
        self.labels.to_owned()
    }

    pub fn annotations(&self) -> HashMap<String, String> {
        self.annotations.to_owned()
    }

    pub fn duration(&self) -> Option<String> {
        self.duration.to_owned()
    }
}
impl default::Default for Config {
    fn default() -> Config {
        Config {
            rules_files: Vec::new(),
            output: String::from("-"),
            meta_alerting_name: String::from("AlertingMetaAlerts"),
            metric_absent_prefix: String::from("metric_absent"),
            labels: [("service", "alerting"), ("severity", "warning")]
                .iter()
                .cloned()
                .map(|(k, v)| (String::from(k), String::from(v)))
                .collect(),
            annotations: [
                ("title", "Metric used in alert is `absent`!"),
                (
                    "description",
                    "The metric `{metric}` is `absent`! Go check the relevant exporter for updates",
                ),
            ]
            .iter()
            .cloned()
            .map(|(k, v)| (String::from(k), String::from(v)))
            .collect(),
            duration: Some(String::from("10m")),
        }
    }
}
