use std::collections::HashMap;

static KEY_VALUE_ARG_DELIMITER: &str = "=";
static METRIC_VARIABLE_REPLACEMENT_PATTERN: &str = "{metric}";

pub fn replace_metric_name(
    map: HashMap<String, String>,
    metric_name: &str,
) -> HashMap<String, String> {
    map.iter()
        .map(|(key, val)| {
            (
                key.to_string(),
                val.replace(METRIC_VARIABLE_REPLACEMENT_PATTERN, metric_name)
                    .to_string(),
            )
        })
        .collect()
}

pub fn divide_key_value_arg(arg: &str) -> Option<(String, String)> {
    let parts: Vec<&str> = arg.split(KEY_VALUE_ARG_DELIMITER).collect();
    match parts.len() {
        2 => Some((parts[0].to_string(), parts[1].to_string())),
        _ => None,
    }
}

#[cfg(test)]
mod test_replace_metric_name {
    use super::*;

    #[test]
    fn should_replace_metric_when_needed() {
        let map: HashMap<String, String> = [
            ("foo", "This should be kept as it is!"),
            ("bar", "Metric: `{metric}`"),
        ]
        .iter()
        .cloned()
        .map(|(k, v)| (String::from(k), String::from(v)))
        .collect();
        let metric_name = "foobarmetric";
        let expected: HashMap<String, String> = [
            ("foo", "This should be kept as it is!"),
            ("bar", "Metric: `foobarmetric`"),
        ]
        .iter()
        .cloned()
        .map(|(k, v)| (String::from(k), String::from(v)))
        .collect();
        let result = replace_metric_name(map, metric_name);
        assert_eq!(result, expected);
    }
}

#[cfg(test)]
mod test_divide_key_value_arg {
    use super::*;

    #[test]
    fn should_some_for_split_key_value() {
        let arg = "foo=bar";
        let expected = (String::from("foo"), String::from("bar"));
        let result = divide_key_value_arg(arg).unwrap();
        assert_eq!(result, expected);
    }

    #[test]
    fn should_return_none_for_malformed_arg() {
        let arg = "foobar";
        let result = divide_key_value_arg(arg);
        assert!(result.is_none());
    }
}
