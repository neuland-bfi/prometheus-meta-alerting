mod config;
mod prometheus;
mod utils;

pub use config::Config;

use prometheus::{Alerting, AlertingError, AlertingGroup, AlertingRule, Metric};

pub fn meta_alerting(config: &Config) -> Result<Alerting, AlertingError> {
    let used_metrics = metrics_from_alerting_files(config)?;
    let meta_rules: Vec<AlertingRule> = used_metrics
        .into_iter()
        .map(|m| AlertingRule::from_metric(m, config))
        .collect();
    Ok(Alerting::new(vec![AlertingGroup::meta(meta_rules, config)]))
}

fn metrics_from_alerting_files(config: &Config) -> Result<Vec<Metric>, AlertingError> {
    let mut metrics: Vec<Metric> = Vec::new();
    for file in config.rules_files() {
        metrics.append(&mut Alerting::from_file(&file)?.metrics(config)?);
    }
    metrics.sort();
    metrics.dedup();
    Ok(metrics)
}
