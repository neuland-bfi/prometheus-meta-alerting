use std::fs;
use std::process::exit;

use prometheus_meta_alerting;

fn main() {
    let config = prometheus_meta_alerting::Config::from_cli();
    let alerting = match prometheus_meta_alerting::meta_alerting(&config) {
        Ok(alerting) => alerting,
        Err(e) => {
            eprintln!("Error: {}", e);
            exit(1);
        }
    };
    let alerting_yaml = match alerting.yaml() {
        Ok(yaml) => yaml,
        Err(e) => {
            eprintln!("Error: {}", e);
            exit(1);
        }
    };
    if config.output() == "-" {
        println!("{}", alerting_yaml);
    } else {
        match fs::write(config.output(), alerting_yaml) {
            Ok(_) => {}
            Err(e) => {
                eprintln!("Error writing file: {}", e);
                exit(1);
            }
        }
    }
}
