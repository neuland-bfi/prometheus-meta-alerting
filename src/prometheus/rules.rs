use std::cmp::Ordering;
use std::collections::HashMap;
use std::error::Error;
use std::fmt;
use std::fs::read_to_string;

use serde::{Deserialize, Serialize};
use serde_yaml;

use crate::config::Config;
use crate::config::{ALLOW_PERIODS, IGNORE_META_ALERTING_LABEL};
use crate::utils;

#[derive(Debug)]
pub enum AlertingError {
    FileError(String),
    InvalidYaml(String),
    InvalidExpression(String),
}
impl fmt::Display for AlertingError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::FileError(msg) => f.write_str(&format!("Error opening file: {}", msg)),
            Self::InvalidYaml(msg) => f.write_str(&format!("Error parsing rules yaml: {}", msg)),
            Self::InvalidExpression(msg) => {
                f.write_str(&format!("Error parsing PromQL expression: {}", msg))
            }
        }
    }
}
impl Error for AlertingError {}
impl From<std::io::Error> for AlertingError {
    fn from(e: std::io::Error) -> Self {
        Self::FileError(e.to_string())
    }
}
impl From<serde_yaml::Error> for AlertingError {
    fn from(e: serde_yaml::Error) -> Self {
        Self::InvalidYaml(e.to_string())
    }
}

#[derive(Deserialize, Serialize, Debug, PartialEq)]
pub struct Alerting {
    groups: Vec<AlertingGroup>,
}
impl Alerting {
    pub fn from_file(file_name: &str) -> Result<Self, AlertingError> {
        let content = read_to_string(file_name)?;
        Ok(Self::from_yaml(&content)?)
    }

    fn from_yaml(yaml: &str) -> Result<Self, serde_yaml::Error> {
        serde_yaml::from_str(yaml)
    }

    pub fn new(groups: Vec<AlertingGroup>) -> Self {
        Alerting { groups }
    }

    pub fn metrics(&self, config: &Config) -> Result<Vec<Metric>, AlertingError> {
        let mut metrics: Vec<Metric> = Vec::new();
        for group in &self.groups {
            metrics.append(&mut group.metrics(config)?);
        }
        Ok(metrics)
    }

    pub fn yaml(&self) -> Result<String, serde_yaml::Error> {
        Ok(serde_yaml::to_string(&self)?)
    }
}

#[derive(Deserialize, Serialize, Debug, PartialEq)]
pub struct AlertingGroup {
    name: String,
    rules: Vec<AlertingRule>,
}
impl AlertingGroup {
    #[cfg(test)]
    fn new(name: &str, rules: Vec<AlertingRule>) -> Self {
        let name = name.to_string();
        AlertingGroup { name, rules }
    }

    pub fn meta(rules: Vec<AlertingRule>, config: &Config) -> Self {
        let name = String::from(config.meta_alerting_name());
        AlertingGroup { name, rules }
    }

    fn metrics(&self, config: &Config) -> Result<Vec<Metric>, AlertingError> {
        let mut metrics: Vec<Metric> = Vec::new();
        // ignore meta alerting
        if self.name != config.meta_alerting_name() {
            for rule in &self.rules {
                metrics.append(&mut rule.metrics()?);
            }
        }
        Ok(metrics)
    }
}

#[derive(Deserialize, Serialize, Debug, PartialEq)]
pub struct AlertingRule {
    alert: String,
    expr: Expression,
    #[serde(rename = "for", skip_serializing_if = "Option::is_none")]
    duration: Option<String>,
    labels: Option<HashMap<String, String>>,
    annotations: Option<HashMap<String, String>>,
}
impl AlertingRule {
    #[cfg(test)]
    fn new(
        alert: &str,
        expr: Expression,
        labels: Option<HashMap<String, String>>,
        annotations: Option<HashMap<String, String>>,
        duration: Option<String>,
    ) -> Self {
        let alert = alert.to_string();
        AlertingRule {
            alert,
            expr,
            labels,
            annotations,
            duration,
        }
    }

    pub fn metrics(&self) -> Result<Vec<Metric>, AlertingError> {
        // `IGNORE_META_ALERTING_LABEL` set to _anything_? ignore this
        if let Some(labels) = &self.labels {
            if labels.contains_key(IGNORE_META_ALERTING_LABEL) {
                return Ok(Vec::new());
            }
        };
        Ok(self.expr.metrics()?)
    }

    pub fn from_metric(metric: Metric, config: &Config) -> Self {
        let alert = format!("{}_{}", config.metric_absent_prefix(), metric);
        let expr = Expression::new(&metric.absent_query());
        let labels = utils::replace_metric_name(config.labels(), metric.name());
        let annotations = utils::replace_metric_name(config.annotations(), metric.name());
        let duration = config.duration();
        Self {
            alert,
            expr,
            labels: Some(labels),
            annotations: Some(annotations),
            duration,
        }
    }
}

#[derive(Deserialize, Serialize, Debug, PartialEq)]
struct Expression(String);
impl Expression {
    fn new(expr: &str) -> Self {
        let expr = expr.to_string();
        Expression(expr)
    }

    pub fn metrics(&self) -> Result<Vec<Metric>, AlertingError> {
        let mut metrics: Vec<Metric> = Vec::new();
        let ast: promql::Node = match promql::parse(self.0.as_bytes(), ALLOW_PERIODS) {
            Ok(ast) => ast,
            Err(_) => return Err(AlertingError::InvalidExpression(self.0.clone())),
        };
        Expression::extract_metrics(ast, &mut metrics);
        metrics.sort();
        metrics.dedup();
        Ok(metrics)
    }

    fn extract_metrics(node: promql::Node, buf: &mut Vec<Metric>) {
        match node {
            promql::Node::Operator { x, op: _, y } => {
                Expression::extract_metrics(*x, buf);
                Expression::extract_metrics(*y, buf);
            }
            promql::Node::Function {
                name: _,
                args,
                aggregation: _,
            } => {
                for arg in args {
                    Expression::extract_metrics(arg, buf);
                }
            }
            promql::Node::Vector(v) => {
                for label in v.labels {
                    if label.name == "__name__" {
                        buf.push(Metric::new(&label.value));
                    }
                }
            }
            _ => {}
        }
    }
}

#[derive(Deserialize, Serialize, Debug, PartialEq, PartialOrd)]
pub struct Metric(String);
impl Metric {
    fn new(name: &str) -> Self {
        let name = name.to_string();
        Metric(name)
    }

    fn absent_query(&self) -> String {
        format!("absent({}) == 1", self.0)
    }

    pub fn name(&self) -> &str {
        &self.0
    }
}
impl Ord for Metric {
    fn cmp(&self, other: &Self) -> Ordering {
        self.0.cmp(&other.0)
    }
}
impl Eq for Metric {}
impl fmt::Display for Metric {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_str(&self.0)
    }
}

#[cfg(test)]
mod test_alerting_from_yaml {
    use super::*;

    #[test]
    fn should_deserialize_rules_file_content_into_alerting_struct() {
        let yaml = r#"---
groups:
  - name: test_alerts
    rules:
      - alert: Test
        expr: some_test_metric"#;
        let expected = Alerting::new(vec![AlertingGroup::new(
            "test_alerts",
            vec![AlertingRule::new(
                "Test",
                Expression(String::from("some_test_metric")),
                None,
                None,
                None,
            )],
        )]);
        let result = Alerting::from_yaml(yaml).unwrap();
        assert_eq!(result, expected);
    }
}

#[cfg(test)]
mod test_expression_metrics {
    use super::*;

    #[test]
    fn should_extract_single_metric() {
        let expression = Expression::new("sum(some_metric{foo=\"bar\"}) >= 42");
        let expected = vec![Metric::new("some_metric")];
        let result = expression.metrics().unwrap();
        assert_eq!(result, expected);
    }

    #[test]
    fn should_extract_multiple_metrics() {
        let expression = Expression::new("sum(some_metric{foo=\"bar\"}) >= sum(other_metric)");
        let expected = vec![Metric::new("other_metric"), Metric::new("some_metric")];
        let result = expression.metrics().unwrap();
        assert_eq!(result, expected);
    }

    #[test]
    fn should_include_duplicated_metric_once() {
        let expression =
            Expression::new("sum(some_metric{foo=\"bar\"}) >= sum(some_metric{bar=\"foo\"})");
        let expected = vec![Metric::new("some_metric")];
        let result = expression.metrics().unwrap();
        assert_eq!(result, expected);
    }

    #[test]
    fn should_return_error_for_invalid_expression() {
        let expression = Expression::new("/");
        assert!(expression.metrics().is_err());
    }
}

#[cfg(test)]
mod test_alerting_rule_from_metric {
    use super::*;

    #[test]
    fn should_have_meta_alerting_name() {
        let config = Config::default();
        let metric = Metric::new("some_metric");
        let rule = AlertingRule::from_metric(metric, &config);
        let expected = "metric_absent_some_metric";
        assert_eq!(rule.alert, expected);
    }

    #[test]
    fn should_have_expression_checking_for_absence_of_metric() {
        let config = Config::default();
        let metric = Metric::new("some_metric");
        let rule = AlertingRule::from_metric(metric, &config);
        let expected = Expression::new("absent(some_metric) == 1");
        assert_eq!(rule.expr, expected);
    }

    #[test]
    fn should_have_duration_from_config() {
        let config = Config::default();
        let metric = Metric::new("some_metric");
        let rule = AlertingRule::from_metric(metric, &config);
        let expected = Some(String::from("10m"));
        assert_eq!(rule.duration, expected);
    }

    #[test]
    fn should_have_no_duration_if_config_has_none() {
        let config = Config::new(
            Vec::new(),
            String::from("-"),
            String::from("AlertingMetaAlerts"),
            String::from("metric_absent"),
            HashMap::new(),
            HashMap::new(),
            None,
        );
        let metric = Metric::new("some_metric");
        let rule = AlertingRule::from_metric(metric, &config);
        assert!(rule.duration.is_none());
    }
}

#[cfg(test)]
mod test_alerting_rule_metrics {
    use super::*;

    #[test]
    fn should_find_metrics_used_in_alerting_rule() {
        let rule = AlertingRule::new(
            "test_alert",
            Expression::new("foo(bar)"),
            Some(
                [
                    (String::from("service"), String::from("test")),
                    (String::from("severity"), String::from("warning")),
                ]
                .iter()
                .cloned()
                .collect(),
            ),
            Some(HashMap::new()),
            None,
        );
        let expected = vec![Metric::new("bar")];
        let result: Vec<Metric> = rule.metrics().unwrap();
        assert_eq!(result, expected);
    }

    #[test]
    fn should_ignore_alerting_rule_when_labeled_as_ignore() {
        let rule = AlertingRule::new(
            "test_alert",
            Expression::new("foo(bar)"),
            Some(
                [
                    (String::from("service"), String::from("test")),
                    (String::from("severity"), String::from("warning")),
                    (String::from("ignore_meta_alerting"), String::from("true")),
                ]
                .iter()
                .cloned()
                .collect(),
            ),
            Some(HashMap::new()),
            None,
        );
        let expected: Vec<Metric> = Vec::new();
        let result: Vec<Metric> = rule.metrics().unwrap();
        assert_eq!(result, expected);
    }
}

#[cfg(test)]
mod test_alerting_group_metrics {
    use super::*;

    #[test]
    fn should_find_metrics_used_in_alerting_group() {
        let config = Config::default();
        let group = AlertingGroup::new(
            "TestAlerts",
            vec![
                AlertingRule::new(
                    "test_alert",
                    Expression::new("foo(bar)"),
                    Some(
                        [
                            (String::from("service"), String::from("test")),
                            (String::from("severity"), String::from("warning")),
                        ]
                        .iter()
                        .cloned()
                        .collect(),
                    ),
                    Some(HashMap::new()),
                    None,
                ),
                AlertingRule::new(
                    "another_test_alert",
                    Expression::new("some_test_metric"),
                    Some(
                        [
                            (String::from("service"), String::from("test")),
                            (String::from("severity"), String::from("warning")),
                        ]
                        .iter()
                        .cloned()
                        .collect(),
                    ),
                    Some(HashMap::new()),
                    None,
                ),
            ],
        );
        let expected = vec![Metric::new("bar"), Metric::new("some_test_metric")];
        let result: Vec<Metric> = group.metrics(&config).unwrap();
        assert_eq!(result, expected);
    }

    #[test]
    fn should_ignore_metrics_from_meta_alerting_group() {
        let config = Config::default();
        let group = AlertingGroup::new(
            config.meta_alerting_name(),
            vec![AlertingRule::new(
                "test_alert",
                Expression::new("foo(bar)"),
                Some(
                    [
                        (String::from("service"), String::from("test")),
                        (String::from("severity"), String::from("warning")),
                    ]
                    .iter()
                    .cloned()
                    .collect(),
                ),
                Some(HashMap::new()),
                None,
            )],
        );
        let expected = Vec::new();
        let result: Vec<Metric> = group.metrics(&config).unwrap();
        assert_eq!(result, expected);
    }
}
