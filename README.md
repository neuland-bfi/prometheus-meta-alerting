# Prometheus Meta Alerting
This tool looks through your [Prometheus alerts](https://www.prometheus.io/docs/prometheus/latest/configuration/alerting_rules/) which metrics are used, and generates `absent()`-alerts for all of the metrics used in your alerting.

## Why?
Sometimes exporters change their metrics with an update.
We want to know when metrics disappear.

## Why Rust?
Rust is the only language I know sufficiently well for which I found a [lib to parse PromQL expressions](https://github.com/vthriller/promql).

## Usage
To collect your alerting rules and write generated meta alerting to stdout, you can do
```
prometheus-meta-alerting /path/to/your/rules/*.yml -
```
For more detailed CLI usage instructions see `prometheus-meta-alerting --help`.

This tool generates a Prometheus alerting group named `AlertingMetaAlerts`.
You can safely pass this a file with your meta alerts to this tool as it will ignore this group.

### Configuration
Some things, like the alerting group name, an alert name prefix, or the `for` value used for the alert, can be overridden using command line options.  
See the output of `--help` for more information.

### Labels & Annotations
You can choose to manually ignore specific alerting rules, by adding an `ignore_meta_alerting` label.
The actual value you use is irrelevant (`"true"` may be the clearest); as long as the label is present, the rule will be ignored.

Using the `--labels foo=bar` and `--annotation foo=bar` options, you can also specify additional labels and annotations, or override the defaults.  
Including `{metric}` in a custom label- or annotation value allows you to include the metric name:
```
prometheus-meta-alerting /your/alerts/*.yml - --annotation title='Metric is `absent`: `{metric}`!'
```
will replace the default `title` annotation and will have the metric name in its value.

In case you're using something _like_ the default annotations or labels, but named it differently, you can exclude the defaults to avoid duplicated annotations and labels.  
To do that, use the `--exclude-default-annotations` or `--exclude-default-labels` flags.

## Building
First, make sure you have [Rust installed](https://www.rust-lang.org/tools/install).
Then you can use `cargo` to build, test, etc.:
```
cargo test
cargo build
```

The pre-compiled binaries in the GitLab releases are built using
```
cargo build --release
```

In most releases there should be two binaries - one dynamically linked and one statically linked.  
The statically linked binary is built using
```
rustup target add x86_64-unknown-linux-musl
cargo build --target x86_64-unknown-linux-musl --release
```

See the [docs for building fully statically linked binaries](https://doc.rust-lang.org/edition-guide/rust-2018/platform-and-target-support/musl-support-for-fully-static-binaries.html) for more information.
