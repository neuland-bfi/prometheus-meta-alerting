use std::fs::{remove_file, write};
use std::process::Command;

#[test]
fn should_not_panic_when_showing_usage() {
    // this is created by running `cargo test`
    Command::new("target/debug/prometheus-meta-alerting")
        .arg("--help")
        .output()
        .expect("failed to execute process");
}

#[test]
fn should_have_custom_labels_in_output() {
    write(
        "/tmp/prometheus-meta-alerting-test-should-have-custom-labels-in-output.yml",
        r#"---
groups:
  - name: some_test_alerts
    rules:
      - alert: FooBar
        expr: foo_bar > 42"#,
    )
    .unwrap();

    // this is created by running `cargo test`
    let output: String = String::from_utf8(
        Command::new("target/debug/prometheus-meta-alerting")
            .arg("/tmp/prometheus-meta-alerting-test-should-have-custom-labels-in-output.yml")
            .arg("-")
            .arg("--label")
            .arg("foo=bar")
            .output()
            .unwrap()
            .stdout,
    )
    .unwrap();
    println!("output: {}", output);

    assert!(output.contains("name: AlertingMetaAlerts"));
    assert!(output.contains("alert: metric_absent_foo_bar"));
    assert!(output.contains("expr: absent(foo_bar) == 1"));
    assert!(output.contains("service: alerting"));
    assert!(output.contains("foo: bar"));

    remove_file("/tmp/prometheus-meta-alerting-test-should-have-custom-labels-in-output.yml")
        .unwrap();
}

#[test]
fn should_have_custom_annotations_in_output() {
    write(
        "/tmp/prometheus-meta-alerting-test-should-have-custom-annotations-in-output.yml",
        r#"---
groups:
  - name: some_test_alerts
    rules:
      - alert: FooBar
        expr: foo_bar > 42"#,
    )
    .unwrap();

    // this is created by running `cargo test`
    let output: String = String::from_utf8(
        Command::new("target/debug/prometheus-meta-alerting")
            .arg("/tmp/prometheus-meta-alerting-test-should-have-custom-annotations-in-output.yml")
            .arg("-")
            .arg("--annotation")
            .arg("foo=bar")
            .output()
            .unwrap()
            .stdout,
    )
    .unwrap();

    assert!(output.contains("name: AlertingMetaAlerts"));
    assert!(output.contains("alert: metric_absent_foo_bar"));
    assert!(output.contains("expr: absent(foo_bar) == 1"));
    assert!(output.contains("service: alerting"));
    assert!(output.contains("foo: bar"));

    remove_file("/tmp/prometheus-meta-alerting-test-should-have-custom-annotations-in-output.yml")
        .unwrap();
}

#[test]
fn should_override_default_annotation() {
    write(
        "/tmp/prometheus-meta-alerting-test-should-override-default-annotation.yml",
        r#"---
groups:
  - name: some_test_alerts
    rules:
      - alert: FooBar
        expr: foo_bar > 42"#,
    )
    .unwrap();

    // this is created by running `cargo test`
    let output: String = String::from_utf8(
        Command::new("target/debug/prometheus-meta-alerting")
            .arg("/tmp/prometheus-meta-alerting-test-should-override-default-annotation.yml")
            .arg("-")
            .arg("--annotation")
            .arg("description=Metric: `{metric}`")
            .output()
            .unwrap()
            .stdout,
    )
    .unwrap();

    assert!(output.contains("name: AlertingMetaAlerts"));
    assert!(output.contains("alert: metric_absent_foo_bar"));
    assert!(output.contains("expr: absent(foo_bar) == 1"));
    assert!(output.contains("description: \"Metric: `foo_bar`\""));

    remove_file("/tmp/prometheus-meta-alerting-test-should-override-default-annotation.yml")
        .unwrap();
}

#[test]
fn should_override_default_label() {
    write(
        "/tmp/prometheus-meta-alerting-test-should-override-default-label.yml",
        r#"---
groups:
  - name: some_test_alerts
    rules:
      - alert: FooBar
        expr: foo_bar > 42"#,
    )
    .unwrap();

    // this is created by running `cargo test`
    let output: String = String::from_utf8(
        Command::new("target/debug/prometheus-meta-alerting")
            .arg("/tmp/prometheus-meta-alerting-test-should-override-default-label.yml")
            .arg("-")
            .arg("--label")
            .arg("service=foobar")
            .output()
            .unwrap()
            .stdout,
    )
    .unwrap();

    assert!(output.contains("name: AlertingMetaAlerts"));
    assert!(output.contains("alert: metric_absent_foo_bar"));
    assert!(output.contains("expr: absent(foo_bar) == 1"));
    assert!(output.contains("service: foobar"));

    remove_file("/tmp/prometheus-meta-alerting-test-should-override-default-label.yml").unwrap();
}

#[test]
fn should_override_alerting_name_and_metric_prefix() {
    write(
        "/tmp/prometheus-meta-alerting-test-should-override-alerting-name-and-metric-prefix.yml",
        r#"---
groups:
  - name: some_test_alerts
    rules:
      - alert: FooBar
        expr: foo_bar > 42"#,
    )
    .unwrap();

    // this is created by running `cargo test`
    let output: String = String::from_utf8(
        Command::new("target/debug/prometheus-meta-alerting")
            .arg("/tmp/prometheus-meta-alerting-test-should-override-alerting-name-and-metric-prefix.yml")
            .arg("-")
            .arg("--meta-alerting-name")
            .arg("MetaAlertingTest")
            .arg("--metric-absent-prefix")
            .arg("absent")
            .output()
            .unwrap()
            .stdout,
    )
    .unwrap();

    assert!(output.contains("name: MetaAlertingTest"));
    assert!(output.contains("alert: absent_foo_bar"));

    remove_file(
        "/tmp/prometheus-meta-alerting-test-should-override-alerting-name-and-metric-prefix.yml",
    )
    .unwrap();
}

#[test]
fn should_exclude_default_labels_when_disabling_via_cli() {
    write(
        "/tmp/prometheus-meta-alerting-test-should-exclude_default_labels_when_disabling_via_cli.yml",
        r#"---
groups:
  - name: some_test_alerts
    rules:
      - alert: FooBar
        expr: foo_bar > 42"#,
    )
    .unwrap();

    // this is created by running `cargo test`
    let output: String = String::from_utf8(
        Command::new("target/debug/prometheus-meta-alerting")
            .arg("/tmp/prometheus-meta-alerting-test-should-exclude_default_labels_when_disabling_via_cli.yml")
            .arg("-")
            .arg("--exclude-default-labels")
            .arg("--label")
            .arg("foo=bar")
            .output()
            .unwrap()
            .stdout,
    )
    .unwrap();
    println!("output: {}", output);

    assert!(output.contains("foo: bar"));
    assert!(!output.contains("severity: warning"));
    assert!(!output.contains("service: alerting"));

    remove_file(
        "/tmp/prometheus-meta-alerting-test-should-exclude_default_labels_when_disabling_via_cli.yml",
    )
    .unwrap();
}

#[test]
fn should_exclude_default_annotations_when_disabling_via_cli() {
    write(
        "/tmp/prometheus-meta-alerting-test-should-exclude_default_annotations_when_disabling_via_cli.yml",
        r#"---
groups:
  - name: some_test_alerts
    rules:
      - alert: FooBar
        expr: foo_bar > 42"#,
    )
    .unwrap();

    // this is created by running `cargo test`
    let output: String = String::from_utf8(
        Command::new("target/debug/prometheus-meta-alerting")
            .arg("/tmp/prometheus-meta-alerting-test-should-exclude_default_annotations_when_disabling_via_cli.yml")
            .arg("-")
            .arg("--exclude-default-annotations")
            .arg("--annotation")
            .arg("foo=bar")
            .output()
            .unwrap()
            .stdout,
    )
    .unwrap();
    println!("output: {}", output);

    assert!(output.contains("foo: bar"));
    assert!(!output.contains("title: \"Metric used in alert is `absent`!\""));
    assert!(!output.contains("description: \"The metric `foo_bar` is `absent`! Go check the relevant exporter for updates\""));

    remove_file(
        "/tmp/prometheus-meta-alerting-test-should-exclude_default_annotations_when_disabling_via_cli.yml",
    )
    .unwrap();
}

#[test]
fn should_have_empty_labels_when_disabling_via_cli_without_custom() {
    write(
        "/tmp/prometheus-meta-alerting-test-should-have-empty-labels-when-disabling-via-cli-without-custom.yml",
        r#"---
groups:
  - name: some_test_alerts
    rules:
      - alert: FooBar
        expr: foo_bar > 42"#,
    )
    .unwrap();

    // this is created by running `cargo test`
    let output: String = String::from_utf8(
        Command::new("target/debug/prometheus-meta-alerting")
            .arg("/tmp/prometheus-meta-alerting-test-should-have-empty-labels-when-disabling-via-cli-without-custom.yml")
            .arg("-")
            .arg("--exclude-default-labels")
            .output()
            .unwrap()
            .stdout,
    )
    .unwrap();
    println!("output: {}", output);

    assert!(output.contains("labels: {}"));
    assert!(!output.contains("severity: warning"));
    assert!(!output.contains("service: alerting"));

    remove_file(
        "/tmp/prometheus-meta-alerting-test-should-have-empty-labels-when-disabling-via-cli-without-custom.yml",
    )
    .unwrap();
}

#[test]
fn should_have_empty_annotations_when_disabling_via_cli_without_custom() {
    write(
        "/tmp/prometheus-meta-alerting-test-should-have-empty-annotations-when-disabling-via-cli-without-custom.yml",
        r#"---
groups:
  - name: some_test_alerts
    rules:
      - alert: FooBar
        expr: foo_bar > 42"#,
    )
    .unwrap();

    // this is created by running `cargo test`
    let output: String = String::from_utf8(
        Command::new("target/debug/prometheus-meta-alerting")
            .arg("/tmp/prometheus-meta-alerting-test-should-have-empty-annotations-when-disabling-via-cli-without-custom.yml")
            .arg("-")
            .arg("--exclude-default-annotations")
            .output()
            .unwrap()
            .stdout,
    )
    .unwrap();
    println!("output: {}", output);

    assert!(output.contains("annotations: {}"));
    assert!(!output.contains("title: \"Metric used in alert is `absent`!\""));
    assert!(!output.contains("description: \"The metric `foo_bar` is `absent`! Go check the relevant exporter for updates\""));

    remove_file(
        "/tmp/prometheus-meta-alerting-test-should-have-empty-annotations-when-disabling-via-cli-without-custom.yml",
    )
    .unwrap();
}

#[test]
fn should_have_custom_for_when_passing_via_cli() {
    write(
        "/tmp/prometheus-meta-alerting-test-should-have-custom-for-when-passing-via-cli.yml",
        r#"---
groups:
  - name: some_test_alerts
    rules:
      - alert: FooBar
        expr: foo_bar > 42"#,
    )
    .unwrap();

    // this is created by running `cargo test`
    let output: String = String::from_utf8(
        Command::new("target/debug/prometheus-meta-alerting")
            .arg("/tmp/prometheus-meta-alerting-test-should-have-custom-for-when-passing-via-cli.yml")
            .arg("-")
            .arg("--for")
            .arg("42m")
            .output()
            .unwrap()
            .stdout,
    )
    .unwrap();
    println!("output: {}", output);

    assert!(output.contains("for: 42m"));

    remove_file(
        "/tmp/prometheus-meta-alerting-test-should-have-custom-for-when-passing-via-cli.yml",
    )
    .unwrap();
}

#[test]
fn should_exclude_for_when_disabling_via_cli() {
    write(
        "/tmp/prometheus-meta-alerting-test-should-exclude-for-when-passing-empty-string-via-cli.yml",
        r#"---
groups:
  - name: some_test_alerts
    rules:
      - alert: FooBar
        expr: foo_bar > 42"#,
    )
    .unwrap();

    let strings_to_disable = vec!["", "-"];
    for s in strings_to_disable {
        // this is created by running `cargo test`
        let output: String = String::from_utf8(
            Command::new("target/debug/prometheus-meta-alerting")
                .arg("/tmp/prometheus-meta-alerting-test-should-exclude-for-when-passing-empty-string-via-cli.yml")
                .arg("-")
                .arg("--for")
                .arg(s)
                .output()
                .unwrap()
                .stdout,
        )
        .unwrap();
        println!("output: {}", output);

        println!("tested string to disable `for`: {}", s);
        assert!(!output.contains("for:"));
    }

    remove_file(
        "/tmp/prometheus-meta-alerting-test-should-exclude-for-when-passing-empty-string-via-cli.yml",
    )
    .unwrap();
}
